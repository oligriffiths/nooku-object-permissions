<?php
/**
 * User: oli griffiths
 * Date: 05/08/2013
 * Time: 10:19
 */

namespace Nooku\Component\Events;

use Nooku\Library;
use Nooku\Library\DatabaseRowInterface;

/**
 * Class DecoratorPermissionsRow
 *
 * DatabaseRow specific extension of the abstract permissions class.
 * Adds additional methods and the DatabaseRowInterface of DatabaseRows
 *
 * @package Nooku\Component\Events
 */
class DecoratorPermissionsRow extends DecoratorPermissionsAbstract implements Library\DatabaseRowInterface
{
    /**
     * If the delegate has a user_id or created_by property the user id property is set accordingly
     * If the user id is set, the current users group is used for the group id
     *
     * @param ObjectDecoratable $delegate
     */
    public function onDecorate(Library\ObjectDecoratable $delegate)
    {
        parent::onDecorate($delegate);

        $user_id = $this->_user_id ?: (isset($delegate->user_id) ? $delegate->user_id : (isset($delegate->created_by) ? $delegate->created_by : null));
        if($user_id){
            $user               = $this->getObject('com:users.model.users')->id($user_id)->getRow();
            $this->_user_id     = $user->id;
            if(!$this->_group_id)
                $this->_group_id    = $user->getRole()->id;
        }
    }


    /**
     * Returns the readable properties
     *
     * @param bool $modified
     * @return array
     */
    public function getData($modified = false, $force = false)
    {
        $data = $this->getDelegate()->getData($modified);
        $newData = array();
        foreach($data AS $column => $value){
            if($force || $this->canRead($column)) $newData[$column] = $value;
        }

        return $newData;
    }

    /**
     * Sets the writeable properties
     *
     * @param mixed $data
     * @param bool $modified
     * @return $this|DatabaseRowInterface
     */
    public function setData($data, $modified = true, $force = false)
    {
        if ($data instanceof DatabaseRowInterface) {
            $data = $data->toArray();
        } else {
            $data = (array)$data;
        }

        if ($modified)
        {
            foreach ($data as $column => $value) {
                $this->set($column, $value, $force);
            }
        }
        else {
            //If the data isn't being modified, it's a force set, thus ignore perms
            $this->getDelegate()->setData($data, false);
        }

        return $this;
    }

    /**
     * Gets a property if it can be read
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * Sets a property if it can be written
     * @param mixed $offset
     * @param mixed $value
     * @return mixed|void
     */
    public function offsetSet($offset, $value)
    {
        return $this->set($offset, $value);
    }

    /**
     * Unsets a property if it can be removed
     * @param mixed $offset
     * @return Library\DatabaseRowAbstract|void
     */
    public function offsetUnset($offset)
    {
        return $this->remove($offset);
    }

    /**
     * Emulate the isX functionality of a row, eg isSluggable()
     *
     * @param string $method
     * @param array $arguments
     * @return bool|mixed
     */
    public function __call($method, $arguments)
    {
        $methods = $this->getMethods();
        if(!in_array($method, $methods)){
            $parts = Library\StringInflector::explode($method);

            //Check if a behavior is mixed
            if ($parts[0] == 'is' && isset($parts[1]))
            {
                //Lazy mix behaviors
                $behavior = strtolower($parts[1]);

                if ($this->getDelegate()->getTable()->hasBehavior($behavior)) {
                    $this->getDelegate()->mixin($this->getDelegate()->getTable()->getBehavior($behavior));
                } else {
                    return false;
                }
            }
        }

        return parent::__call($method, $arguments);
    }


    /**
     * DatabaseRowInterface methods
     */
    public function getIterator()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function offsetExists($offset)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function serialize()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function unserialize($serialized)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function count()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function has($column)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function getStatus()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function setStatus($status)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function getStatusMessage()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function setStatusMessage($message)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function getModified()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function load()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function save()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function delete()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function isNew()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function reset($force = true)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function isModified($column)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    public function isConnected()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }
}