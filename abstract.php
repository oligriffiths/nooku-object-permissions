<?php
/**
 * User: oli griffiths
 * Date: 05/08/2013
 * Time: 10:19
 */

namespace Nooku\Component\Events;

use Nooku\Library;

/**
 * Class DecoratorPermissionsAbstract
 *
 * This package allows permissions to be set against an object's properties.
 * Permissions are defined similar to unix file permissions, in the format:
 *
 *  rrw - Following 'user', 'group', 'world' for each corresponding letter
 *
 * There are 3 permission options:
 *  r: read
 *  w: write
 *  a: all - (read and write)
 *
 * Note, a single property can also be supplied, and will be applied to each target, eg: r == rrr
 *
 * Permissions can also be set using a numeric value that corresponds to a bitmask using the constants below.
 *
 * Permissions can be passed in on instantiation via the config in a "permissions" property, indexed by the property names
 * Permissions can also be set using setPermissions(array) or setPermission($property, $perm)
 *
 * @package Nooku\Component\Events
 */
abstract class DecoratorPermissionsAbstract extends Library\ObjectDecorator
{
    protected $_permissions = array();

    /**
     * Enables exceptions to be thrown if setting a property that is read/write only
     * @var bool|mixed
     */
    protected $_throw_read_exceptions = false;
    protected $_throw_write_exceptions = true;

    protected $_user_id;
    protected $_group_id;

    const USER_R    = 1;
    const USER_W    = 2;
    const USER_A    = 3;
    const GROUP_R   = 4;
    const GROUP_W   = 8;
    const GROUP_A   = 12;
    const WORLD_R   = 16;
    const WORLD_W   = 32;
    const WORLD_A   = 48;

    /**
     * Constructor
     *
     * @param ObjectConfig  $config  A ObjectConfig object with optional configuration options
     * @return ObjectDecorator
     */
    public function __construct(Library\ObjectConfig $config)
    {
        parent::__construct($config);

        $this->_user_id = $config->user_id;
        $this->_group_id = $config->group_id;
        $this->_throw_read_exceptions = $config->throw_read_exceptions;
        $this->_throw_write_exceptions = $config->throw_write_exceptions;
        $this->setPermissions($config->permissions->toArray());
    }

    /**
     * Initializes the options for the object
     *
     * Called from {@link __construct()} as a first step of object instantiation.
     *
     * @param   ObjectConfig $object An optional ObjectConfig object with configuration options
     * @return  void
     */
    public function _initialize(Library\ObjectConfig $config)
    {
        $config->append(array(
            'user_id' => null,
            'group_id' => null,
            'throw_read_exceptions' => false,
            'throw_write_exceptions' => true,
            'permissions' => array()
        ));

        parent::_initialize($config);
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->_user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->_user_id = $user_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->_group_id;
    }

    /**
     * @param int $group_id
     */
    public function setGroupId($group_id)
    {
        $this->_group_id = $group_id;
        return $this;
    }

    /**
     * Permission setters/getters
     */

    /**
     * Gets the permissions
     *
     * @param bool $asString - If true, permissions are returned as a string in the format rrw
     * @return array
     */
    public function getPermissions($asString = false)
    {
        if($asString){
            $permissions = array();
            foreach($this->_permissions AS $key => $value) $permissions[$key] = $this->getPermission($key, true);
            return $permissions;

        }else{
            return $this->_permissions;
        }
    }

    /**
     * Sets the permissions for an array of permissions
     *
     * @param $permissions array - A permissions array, indexed by property
     * @return $this
     */
    public function setPermissions($permissions)
    {
        foreach($permissions AS $property => $perms){
            $this->setPermission($property, $perms);
        }

        return $this;
    }

    /**
     * Returns the permission set for a specific property
     *
     * @param $property
     * @param bool $asString - If true, permissions are returned as a string in the format rrw
     * @return string | null
     */
    public function getPermission($property, $asString = false)
    {
        if(!isset($this->_permissions[$property])) return null;

        if($asString){

            $user = '-';
            $group = '-';
            $world = '-';
            $perm = $this->_permissions[$property];

            if($perm & self::USER_A) $user = 'a';
            else if($perm & self::USER_R) $user = 'r';
            else if($perm & self::USER_W) $user = 'w';

            if($perm & self::GROUP_A) $group = 'a';
            else if($perm & self::GROUP_R) $group = 'r';
            else if($perm & self::GROUP_W) $group = 'w';

            if($perm & self::WORLD_A) $world = 'a';
            else if($perm & self::WORLD_R) $world = 'r';
            else if($perm & self::WORLD_W) $world = 'w';

            return $user.$group.$world;

        }else{
            return $this->_permissions[$property];
        }
    }

    /**
     * Sets a permission for a specific property
     * @param $property - The property the permission applies to
     * @param $permission - Permission string in format rrw or integer (bitwise) value
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setPermission($property, $permission)
    {
        if(is_string($permission))
        {
            $permission = strtolower($permission);

            if(strlen($permission) == 1) $permission = str_repeat($permission, 3);

            if(strlen($permission) != 3){
                throw new \InvalidArgumentException(__CLASS__.'::'.__FUNCTION__.' for property ('.$property.') requires a string permission to be 1 or 3 characters long. '.$permission.' supplied');
            }

            if(preg_match('#[^wra\-]+#', $permission)){
                throw new \InvalidArgumentException(__CLASS__.'::'.__FUNCTION__.' for property ('.$property.') requires a string permission to contain only "w", "r","a" or "-", '.$permission.' supplied');
            }

            $this->_permissions[$property] = $this->_decodePermission($permission);

        }else if(is_int($permission))
        {
            if($permission > 63){
                throw new \InvalidArgumentException(__CLASS__.'::'.__FUNCTION__.' for property ('.$property.') requires an integer permission be less than 64. '.$permission.' supplied');
            }

            if($permission < 0){
                throw new \InvalidArgumentException(__CLASS__.'::'.__FUNCTION__.' for property ('.$property.') requires an integer permission be greater than 0. '.$permission.' supplied');
            }

            $this->_permissions[$property] = $permission;
        }

        return $this;
    }

    /**
     * Decodes a string permission in the format rrw into an integer (bitwise) value
     *
     * @param $permission
     * @return int
     */
    protected function _decodePermission($permission)
    {
        $user = substr($permission, 0, 1);
        $group = substr($permission, 1, 1);
        $world = substr($permission, 2, 1);

        switch($user){
            case 'r': $user = self::USER_R; break;
            case 'w': $user = self::USER_W; break;
            case 'a': $user = self::USER_A; break;
            default: $user = 0; break;
        }

        switch($group){
            case 'r': $group = self::GROUP_R; break;
            case 'w': $group = self::GROUP_W; break;
            case 'a': $group = self::GROUP_A; break;
            default: $group = 0; break;
        }

        switch($world){
            case 'r': $world = self::WORLD_R; break;
            case 'w': $world = self::WORLD_W; break;
            case 'a': $world = self::WORLD_A; break;
            default: $world = 0; break;
        }

        return $user + $group + $world;
    }

    /*
     * Reading/writing properties
     */

    /**
     * Determines if a property can be read
     *
     * @param $property
     * @return bool
     */
    public function canRead($property)
    {
        if(isset($this->_permissions[$property])){

            $perm = $this->_permissions[$property];
            $type = $this->getPermissionType();
            $allowed = true;

            switch($type){
                case 'user':
                    $allowed = (bool) ($perm & self::USER_R);
                    break;

                case 'group':
                    $allowed = (bool) ($perm & self::GROUP_R);
                    break;

                case 'world':
                    $allowed = (bool) ($perm & self::WORLD_R);
                    break;
            }

            if($allowed) return true;
            else return false;

        }else{
            return true;
        }
    }

    /**
     * Determines if a property can be written to
     *
     * @param $property
     * @return bool
     */
    public function canWrite($property)
    {
        if(isset($this->_permissions[$property])){

            $perm = $this->_permissions[$property];
            $type = $this->getPermissionType();
            $allowed = true;

            switch($type){
                case 'user':
                    $allowed = (bool) ($perm & self::USER_W);
                    break;

                case 'group':
                    $allowed = (bool) ($perm & self::GROUP_W);
                    break;

                case 'world':
                    $allowed = (bool) ($perm & self::WORLD_W);
                    break;
            }

            if($allowed) return true;
            else return false;

        }else{
            return true;
        }
    }

    /**
     * Returns the permission type to apply.
     * If $this->_user_id is set, and the current users id matches, "user" is returned
     * If $this->_group_id is set and the current users group matches, "group" is returned
     * Else, "world" is returned
     *
     * @return string
     */
    protected function getPermissionType()
    {
        $user       = $this->getObject('user');
        $user_id    = $user->getId();

        //If current user is owner, check user permission
        if($this->_user_id && $user_id && $user_id == $this->_user_id){
            return 'user';

        //If current user is in same group as the group id
        }else if($this->_group_id && $user->getRole() == $this->_group_id){
            return 'group';

        //Fallback to world permissions
        }else{
            return 'world';
        }
    }

    /**
     * Gets a property from the delegate if it can be read
     *
     * @param $property
     * @return null
     */
    public function get($property, $force = false)
    {
        if($force || $this->canRead($property)) return $this->getDelegate()->$property;
        else if($this->_throw_read_exceptions) throw new \InvalidArgumentException('The property ['.$property.'] is read only', Library\HttpResponse::BAD_REQUEST);
        else return null;
    }

    /**
     * Sets a property on the delegate if it can be written
     *
     * @param $property
     * @param $value
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function set($property, $value, $force = false)
    {
        if($force || $this->canWrite($property)) $this->getDelegate()->$property = $value;
        else if($this->_throw_write_exceptions) throw new \InvalidArgumentException('The property ['.$property.'] is write only', Library\HttpResponse::BAD_REQUEST);
        return $this;
    }

    /**
     * Removes a property if it can be written
     *
     * @param string $column
     * @return Library\DatabaseRowAbstract|void
     */
    public function remove($column, $force = false)
    {
        if($force || $this->canWrite($column)) unset($this->getDelegate()->$column);
        else if($this->_throw_write_exceptions) throw new \BadMethodCallException('THe property ['.$column.'] is write only', Library\HttpResponse::BAD_REQUEST);
    }

    /**
     * Magic get method to return readable properties
     * @param string $key
     * @return mixed|null
     */
    public function __get($key)
    {
        return $this->get($key);
    }

    /**
     * Magic set method to set writable properties
     * @param string $key
     * @param mixed $value
     * @return $this|mixed
     */
    public function __set($key, $value)
    {
        return $this->set($key, $value);
    }

    /**
     * Unsets a property can can be written to
     * @param string $key
     */
    public function __unset($key)
    {
        return $this->remove($key);
    }

    /**
     * Deep clone delegate
     */
    public function __clone()
    {
        $this->_delegate = clone $this->_delegate;
    }
}