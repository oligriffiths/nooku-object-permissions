#Object Permissions Decorator

This package allows permissions to be set against an object's properties.
Permissions are defined similar to unix file permissions, in the format:

    rrw - Following 'user', 'group', 'world' for each corresponding letter

User and group correspond to the owner user and the owner group.

The owner user ID and group ID can be set via config options and setter methods.

    array(
        'user_id' => $user_id,
        'group_id' => $group_id
    )

There are 3 permission options:

 *  r: read
 *  w: write
 *  a: all - (read and write)

Note, a single property can also be supplied, and will be applied to each target, eg: r == rrr

Permissions can also be set using a numeric value that corresponds to a bitmask using the constants:

    DecoratorPermissionsAbstract::USER_R    = 1;
    DecoratorPermissionsAbstract::USER_W    = 2;
    DecoratorPermissionsAbstract::USER_A    = 3;
    DecoratorPermissionsAbstract::GROUP_R   = 4;
    DecoratorPermissionsAbstract::GROUP_W   = 8;
    DecoratorPermissionsAbstract::GROUP_A   = 12;
    DecoratorPermissionsAbstract::WORLD_R   = 16;
    DecoratorPermissionsAbstract::WORLD_W   = 32;
    DecoratorPermissionsAbstract::WORLD_A   = 48;

Permissions can be passed in on instantiation via the config in a "permissions" property, indexed by the property names

Permissions can also be set using setPermissions(array) or setPermission($property, $perm)

##Exceptions

Exceptions will be thrown by default when attempting to set a property, reading a restricted property will return null by default.

This behavior can be altered by passing in some config options:

    array(
        'throw_read_exceptions' => true,
        'throw_write_exceptions' => false
    )

##Configuration

Various configutation options can be passed in:

    array(
        'user_id' => $user_id,              //The owner user id
        'group_id' => $group_id,            //The owner group id
        'throw_read_exceptions' => true,    //Throws exceptions when reading restricted properties
        'throw_write_exceptions' => false,  //Throws exceptions when writing restircted properties
        'permissions' => array(
                                            //Pass key value pairs for properties and their permissions
        )
    )

##Usage

```php
$decorator = $this->getObject('com:events.decorator.permissions.row', array(
    'permissions' => array(
        'slug' => 'r',
        'description' => 'aar',
        'created_on' => 'r',
        'created_by' => 'r',
        'modified_on' => 'r',
        'modified_by' => 'r',
    )
));

$this->getIdentifier()->addDecorator($decorator);

//Permission setters
$decorator->setPermissions(array);
$decorator->setPermission($property, $permission);

//Permission getters
$decorator->getPermissions();
$decorator->getPermission($property);

//Property getters
$decorator->var;
$decorator->get('var', $force); //If $force is true, permission checking is bypassed

//Property setters
$decorator->var = 123;
$decorator->set('var', 123, $force); //If force is true, permission checking is bypassed

//Property unsetters
unset($decorator->var);
$decorator->remove('var', $force); //If force is true, permission checking is bypassed
```

##Test

```php
$test = $test->decorate('com:events.decorator.permissions.default', array('throw_read_exceptions' => true));

$test->setPermissions(array(
    'var1' => 'r',
    'var2' => 'w',
    'var3' => 'a',
    'var4' => '-'
));

try{
    $var1 = $test->var1;
    $test->var1 = 2;
}catch(\Exception $e)
{
    echo $e->getMessage()."\n";
}

try{
    $test->var2 = 5;
    $var2 = $test->var2;
}catch(\Exception $e)
{
    echo $e->getMessage()."\n";
}

try{
    $var3 = $test->var3;
    $test->var3 = 1;
}catch(\Exception $e)
{
    echo $e->getMessage()."\n";
}

try{
    $var4 = $test->var4;
}catch(\Exception $e)
{
    echo $e->getMessage()."\n";
}

try{
    $test->var4 = 4;
}catch(\Exception $e)
{
    echo $e->getMessage()."\n";
}
```